#!/usr/bin/env bash

ssh root@kabila "/opt/flexnetserver/lmutil lmstat -a | grep MAYA_2 | grep license" > /tmp/kabila_maya_license.txt
ssh root@khan "/opt/flexnetserver/lmutil lmstat -a | grep MAYA_2016 | grep license" > /tmp/khan_maya2016_license.txt
ssh root@khan "/opt/flexnetserver/lmutil lmstat -a | grep MAYA_2017 | grep license" > /tmp/khan_maya2017_license.txt
ssh root@khan "/opt/flexnetserver/lmutil lmstat -a | grep MAYA_2018 | grep license" > /tmp/khan_maya2018_license.txt

exit 0
