#!/usr/bin/env python3.6

import discord
from discord.ext import commands
import asyncio
from datetime import datetime as dt
from subprocess import PIPE, run
import os

from config import prefix, token

# token = "NDM5MjY4NzM1NzQ0ODY4MzUy.DcQsxw.C47qc2uSdRCDjOjqKNeycQgTX08"

__basedir__ = os.path.dirname(os.path.realpath(__file__))


class MayaLicense():
    def __init__(self):
        pass

    def maya2016(self):
        """
        output_split[2] : Maya version
        output_split[5] : Total licenses
        output_split[10] : License in use
        """

        # CHECK MAYA 2016 (from kabila)
        cmd = "cat /tmp/kabila_maya_license.txt".split()
        output_split = run(cmd, stdout=PIPE, universal_newlines=True).stdout.split()
        print("DEBUG: output_split = {}".format(output_split))
        maya2016_total = int(output_split[5])
        maya2016_inuse = int(output_split[10])
        # CHECK MAYA 2016 (from khan and sum up with kabila)
        cmd = "cat /tmp/khan_maya2016_license.txt".split()
        output_split = run(cmd, stdout=PIPE, universal_newlines=True).stdout.split()
        print("DEBUG: output_split = {}".format(output_split))
        maya2016_total += int(output_split[5])
        maya2016_inuse += int(output_split[10])
        maya2016_embed = discord.Embed(
            title="__Maya 2016__",
            description="\n\n**License**:\n{}\n\n**In Use**:\n{}".format(maya2016_total, maya2016_inuse),
            colour=0x33cc33)

        return maya2016_total, maya2016_inuse

    def maya2017(self):
        # CHECK MAYA 2017 (from khan)
        cmd = "cat /tmp/khan_maya2017_license.txt".split()
        output_split = run(cmd, stdout=PIPE, universal_newlines=True).stdout.split()
        print("DEBUG: output_split = {}".format(output_split))
        maya2017_total = int(output_split[5])
        maya2017_inuse = int(output_split[10])
        maya2017_embed = discord.Embed(
            title="__Maya 2017__",
            description="\n\n**License**:\n{}\n\n**In Use**:\n{}".format(maya2017_total, maya2017_inuse),
            colour=0x33cc33)

        return maya2017_total, maya2017_inuse

    def maya2018(self):
        # CHECK MAYA 2018 (from khan)
        cmd = "cat /tmp/khan_maya2018_license.txt".split()
        output_split = run(cmd, stdout=PIPE, universal_newlines=True).stdout.split()
        print("DEBUG: output_split = {}".format(output_split))
        maya2018_total = int(output_split[5])
        maya2018_inuse = int(output_split[10])
        maya2018_embed = discord.Embed(
            title="__Maya 2018__",
            description="\n\n**License**:\n{}\n\n**In Use**:\n{}".format(maya2018_total, maya2018_inuse),
            colour=0x33cc33)

        return maya2018_total, maya2018_inuse

    def status(self):
        licenses = list(self.maya2016())
        for item in list(self.maya2017()):
            licenses.append(item)
        for item in list(self.maya2018()):
            licenses.append(item)
        print(licenses)

        output16 = "**__Maya 2016__**\n\n**License**:\n{}\n\n**In Use**:\n{}\n\n".format(licenses[0], licenses[1])
        output17 = "**__Maya 2017__**\n\n**License**:\n{}\n\n**In Use**:\n{}\n\n".format(licenses[2], licenses[3])
        output18 = "**__Maya 2018__**\n\n**License**:\n{}\n\n**In Use**:\n{}\n\n".format(licenses[4], licenses[5])
        link = "For more detailed information, kindly visit http://anima.animapoint/system/maya.php"
        final_output = "{}{}{}{}".format(output16, output17, output18, link)
        maya_embed = discord.Embed(
            description=final_output,
            colour=0x33cc33)

        return maya_embed


class HoudiniLicense():
    def __init__(self):
        pass

    def licenses(self):
        cmd = "cat /tmp/houdini_license.txt".split()
        output = run(cmd, stdout=PIPE, universal_newlines=True).stdout
        houdini_embed = discord.Embed(
            description=output,
            colour=0x33cc33)

        return houdini_embed


class animaBot(discord.Client):
    #    self.maya2016_embed = None
    #    self.maya2016_embed = None
    #    self.maya2016_embed = None

    mayaLicense = MayaLicense()
    houdiniLicense = HoudiniLicense()

    async def on_ready(self):
        print('==============================')
        print("Name\t: {}".format(self.user.name))
        print("ID\t: {}".format(self.user.id))
        print('==============================\n')

        print(__basedir__)

        # while True:
        #    timenow = "{}:{}".format(dt.time.hour, dt.time.minute)
        #    if timenow == "11:53":
        #        # send message to channel

    async def on_message(self, message):
        if message.author.id == self.user.id:
            return

        # PING
        if message.content.startswith(prefix + "ping"):
            # # await client.send_typing(message.channel)
            # await message.channel.send("Pong!")
            await message.channel.send("Pong!")

        # QUOTE TEST
        if message.content.startswith(prefix + "quote"):
            # await client.send_typing(message.channel)
            await message.channel.send("```this is\njust test```")

        if message.content.startswith(prefix + "calendar"):
            # await client.send_typing(message.channel)
            await message.channel.send("!display")

        # EMBED TEST
        if message.content.startswith(prefix + "embed"):
            em = discord.Embed(
                title='My Embed Title',
                description='My Embed Content.',
                url="file://ganglia/sharing",
                colour=0xDEADBF)
            em.set_author(name='Someone', icon_url=client.user.default_avatar_url)
            # await client.send_typing(message.channel)
            await message.channel.send(embed=em)

        # LICENSE CHECKER
        if message.content.startswith(prefix + "license"):
            """
            If no argument given, show help message
            """
            if len(message.content.split()) == 1:
                helpmsg = discord.Embed(
                    title="__License Checker__",
                    description="**Usage:**\n+license <maya | houdini>\n\n**Example:**\n+license maya",
                    colour=0x33cc33)
                await message.channel.send(embed=helpmsg)
                return
            """
            Check for total license and how many in use
            """
            arg = message.content.split()[1]
            if arg == "maya":
                cmd = "/bin/bash {}/shell_scripts/maya_license.sh".format(__basedir__).split()
                run(cmd)
                '''
                cmd = "cat /tmp/kabila_maya_license.txt".split()
                output_split = run(cmd, stdout=PIPE, universal_newlines=True).stdout.split()
                # output_split = output_raw.stdout.split()
                appname = output_split[2].replace(":", "")
                if "2016" in appname:
                    appname = "__Maya 2016__"
                # output = "**Name**:\n{}\n\n**License**:\n{}\n\n**In Use**:\n{}".format(output_split[2].replace(":", ""), output_split[5], output_split[10])
                output = "\n\n**License**:\n{}\n\n**In Use**:\n{}".format(output_split[5], output_split[10])
                em = discord.Embed(
                    title=appname,
                    description=output,
                    colour=0x33cc33)
                '''
                maya_embed = self.mayaLicense.status()
                await message.channel.send(embed=maya_embed)
            elif arg == "houdini":
                # await message.channel.send("Sorry, I haven't been fully groom yet.")
                cmd = "/bin/bash {}/shell_scripts/houdini_license.sh".format(__basedir__).split()
                run(cmd)
                await message.channel.send(embed=self.houdiniLicense.licenses())


client = animaBot()
client.run(token)
