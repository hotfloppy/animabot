#!/usr/bin/env bash

basedir="$(dirname $(readlink -f $0))"

# Install pip package
apt install python3.6 python3-pip -y

# Install virtualenv
pip3 install virtualenv

# Create virtual environment
virtualenv -p python3.6 ${basedir}/venv

# activate virtual environment
source ${basedir}/venv/bin/activate

# Clone discord.py and pip install
git clone -b rewrite https://github.com/Rapptz/discord.py.git ${basedir}/discord.py
pip3 install -U ${basedir}/discord.py/.
pip3 install -U aiohttp
pip3 install -U PyYAML

deactivate

exit 0
