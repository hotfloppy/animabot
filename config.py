import yaml

with open("config.yaml", "r") as stream:
    data_loaded = yaml.load(stream)

prefix = data_loaded['prefix']
token = data_loaded['token']
